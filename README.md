# Netlify Photo Gallery

[![Netlify Status](https://api.netlify.com/api/v1/badges/0da64eff-bb5e-43c6-9d0a-871c19c56b59/deploy-status)](https://app.netlify.com/sites/gaiety-art/deploys)

## Deving Locally

https://gohugo.io/

```
hugo server
```

## Forked from Netlify Photo Gallery

https://github.com/netlify/netlify-photo-gallery/blob/master/README.md/#how-to-deploy-your-own-photo-gallery-with-large-media

Requires Git LFS

